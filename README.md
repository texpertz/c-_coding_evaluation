# Given the following file format:

```
# comment
- YY-MM-DD
  - hhmm: key text1
  - hhmm: key text2
  - hhmm: eod

- YY-MM-DD
  - hhmm: key text2
  - hhmm: key2 text
  - hhmm: key text3
  - hhmm: eod
```

# Format Rules
1. comments are ignored
1. blank lines are ignored
1. hour entries must be under a date
1. hours will be in military format 1700 => 5:00 pm
1. text is optional
1. the minimum key length is 1 character
1. when *key* contains `_` that entry should not be accounted
1. when *key* contains '=' account this entry key as being the same as the previous entry key
1. The `=` key can not be present in the first entry of a date
1. the key '*eod*' indicates the last entry of a day
1. there can be any number of dates in a file, i.e. three months in a file, or a month in three different files
1. dates may be out of sequence
1. there can be any number of files but they will have the same extension
1. the extension will be passed as a command line parameter


# Create a program
that for the accounted logged hours

1. Calculates total hours in the day
1. Print the results in chronological order in the standard output in the following format

- At the standard output
```
YY-MM-DD hh:mm
YY-MM-DD hh:mm
YY-MM-DD hh:mm
YY-MM-DD hh:mm
YY-MM-DD hh:mm
YY-MM-31 hh:mm
YY-MM-01 hh:mm
YY-MM-DD hh:mm
YY-MM-DD hh:mm
YY-MM-DD hh:mm
```

# Keep in mind
- OOA principles
- OOD principles
- OOP principles
- best practices
- language conventions
- other developers will read your code
- you need to maintain your code in the future
- write code can you can be proud of it

# Sample input file #1

```
- 17-07-28
  - 0900: task1 analisys
  - 1200: _ stop
  - 1300: task1 unit test
  - 1400: = coding
  - 1500: = unit test
  - 1600: = coding
  - 1800: eod

- 17-07-31
  - 0900: task1 PR
  - 1000: task2 coding
  - 1200: _ stop
  - 1300: task2 coding
  - 1830: eod

- 17-08-01
  - 0900: task1
  - 1230: _ stop
  - 1300: task1
  - 1500: meeting
  - 1600: task1 coding
  - 1800: eod
```

# Sample result #1

The sample input file #1, after processed by your program, should produce the following result in the standard output of your operating system

```
17-07-28  8:00
17-07-31  8:30
17-08-01  8:30
```
